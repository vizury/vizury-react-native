/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { View, Text, TouchableOpacity, Platform, ScrollView } from 'react-native';
import RnVizuryLogger from 'react-native-rn-vizury-logger';

class App extends React.Component {

  async getFCM() {
    const fcmToken = await RnVizuryLogger.getFCMToken();
    if (__DEV__) console.log("FCM Token:", fcmToken)
  }

  async getAdvertisingId() {
    const advertisingId = await RnVizuryLogger.getAdvertisingId();
    if (__DEV__) console.log("advertisingId :", advertisingId)
  }

  async setIsNotificationAlertEnabled() {
    RnVizuryLogger.setIsNotificationAlertEnabled(true)
  }

  async postFCMToken() {
    //For Referance
    const fcmToken = await RnVizuryLogger.getFCMToken();
    if (Platform.OS === 'ios') {
      RnVizuryLogger.setFCMRegistrationToken(fcmToken)
    } else {
      RnVizuryLogger.setFCMRegistrationToken(fcmToken.fcmToken)
    }
  }

  async addEvent() {
    if (Platform.OS === 'ios') {
      const isCachingEnabled = await RnVizuryLogger.getIsCachingEnabled();
      RnVizuryLogger.addEvent({ param: 'param1', param2: "param2", param4: "param4" }, 'Vizuri Test')
    } else {
      var attributesMap = {};
      attributesMap["pid"] = "AFGEMSBBLL";
      attributesMap["quantity"] = "1";
      attributesMap["price"] = "876";
      attributesMap["category"] = "clothing";
      var sendingMap = {};
      sendingMap["attributes"] = attributesMap;
      RnVizuryLogger.addEvent(sendingMap, 'product page');
    }
  }

  isPushFromVizuryHandler = async () => {
    if (Platform.OS === 'android') {
      let attributesMap = {};
      attributesMap["data"] = "data";
      attributesMap["id"] = "id";
      let sendingMap = {};
      sendingMap["attributes"] = attributesMap;
      const isPushFromVizury = await RnVizuryLogger.isPushFromVizury(sendingMap);
      if (isPushFromVizury.value) {
        RnVizuryLogger.handleNotificationReceived(sendingMap);
      } else {
        // your own logic goes here
      }
    }
  }

  render() {
    return (
      <View style={{ backgroundColor: 'grey', flex: 1, }}>
        <View style={{ flex: 0.03 }}></View>
        <View style={{ flex: 0.82, paddingTop: 20 }}>
          <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 20, color: 'white' }}>Event Logger</Text>
        </View>
        <ScrollView>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity
              style={{ backgroundColor: 'green', width: '90%', alignItems: 'center', height: 45, justifyContent: 'center', borderRadius: 10, marginBottom: 20 }}
              onPress={() => this.getFCM()}
            ><Text style={{ fontSize: 15, textAlign: 'center', color: 'white', fontWeight: 'bold' }}>Get FCM</Text></TouchableOpacity>
            <TouchableOpacity
              style={{ backgroundColor: 'green', width: '90%', alignItems: 'center', height: 45, justifyContent: 'center', borderRadius: 10, marginBottom: 20 }}
              onPress={() => this.getAdvertisingId()}
            ><Text style={{ fontSize: 15, textAlign: 'center', color: 'white', fontWeight: 'bold' }}>Get Advertising Id</Text></TouchableOpacity>

            <TouchableOpacity
              style={{ backgroundColor: 'green', width: '90%', alignItems: 'center', height: 45, justifyContent: 'center', borderRadius: 10, marginBottom: 20 }}
              onPress={() => this.setIsNotificationAlertEnabled()}
            ><Text style={{ fontSize: 15, textAlign: 'center', color: 'white', fontWeight: 'bold' }}>Notification Alert Enabled</Text></TouchableOpacity>

            <TouchableOpacity
              style={{ backgroundColor: 'green', width: '90%', alignItems: 'center', height: 45, justifyContent: 'center', borderRadius: 10, marginBottom: 20 }}
              onPress={() => this.postFCMToken()}
            ><Text style={{ fontSize: 15, textAlign: 'center', color: 'white', fontWeight: 'bold' }}>Post FCM Token</Text></TouchableOpacity>
            <TouchableOpacity
              style={{ backgroundColor: 'green', width: '90%', alignItems: 'center', height: 45, justifyContent: 'center', borderRadius: 10, marginBottom: 20 }}
              onPress={() => this.addEvent()}
            ><Text style={{ fontSize: 15, textAlign: 'center', color: 'white', fontWeight: 'bold' }}>Log Events</Text></TouchableOpacity>

            <TouchableOpacity
              style={{ backgroundColor: 'green', width: '90%', alignItems: 'center', height: 45, justifyContent: 'center', borderRadius: 10, marginBottom: 20 }}
              onPress={() => this.isPushFromVizuryHandler()}
            ><Text style={{ fontSize: 15, textAlign: 'center', color: 'white', fontWeight: 'bold' }}>Handle Notifications</Text></TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    )
  }
}
export default App;