# VizuryEventLogger Example



Vizury is a commerce marketing platform and its personalized retargeting stack is used by digital companies to grow marketing ROI and enhance transactions.

## To run example application

Go to project folder run `npm install`

For iOS please run these commands

```
$ cd ios
$ pod install
```


Note :- 

Please run on real device. 

To Archieve Please go to `Enable Bitcode` and set it `No` for project folder. Same for the `react-native-rn-vizury-logger` and `VizuryEventLoggerReact` in Pod files. 




### Integration Summary 

## Summary


This is npm package to integrate Vizury SDK in react-native. The SDK supports iOS12 and above. 

Open terminal and go to project folder and run below commands


## Install via NPM

`$ npm install react-native-rn-vizury-logger`

### Mostly automatic installation

`$ react-native link react-native-rn-vizury-logger`



## Usage
```javascript
import RnVizuryLogger from 'react-native-rn-vizury-logger';


//Get FCM Token

 const fcmToken = await RnVizuryLogger.getFCMToken();
    if (__DEV__) console.log("FCM Token:", fcmToken)

//Get Advertising ID

const advertisingId = await RnVizuryLogger.getAdvertisingId();
    if (__DEV__) console.log("advertisingId :", advertisingId)


// Set Is Notification Alert Enabled

RnVizuryLogger.setIsNotificationAlertEnabled(true)


//Post FCM Token from React Native

  if (Platform.OS === 'ios') {

      RnVizuryLogger.setFCMRegistrationToken(fcmToken)
    }else{
      RnVizuryLogger.setFCMRegistrationToken(fcmToken.fcmToken)

    }


//Event Logging

 if (Platform.OS === 'ios'){
      RnVizuryLogger.addEvent({ param: 'param1', param2: "param2", param4: "param4" }, 'Vizuri Test')

    }else{
      var attributesMap = {};
      attributesMap["pid"] = "AFGEMSBBLL";
      attributesMap["quantity"] = "1";
      attributesMap["price"] = "876";
      attributesMap["category"] = "clothing";
      var sendingMap = {};
      sendingMap["attributes"] = attributesMap;
      RnVizuryLogger.addEvent(sendingMap, 'product page');

    }
```  
## RN Firebase

This method is put inside "setBackgroundMessageHandler"
```
// a function to pass notification payload to vizury sdk for android
    isPushFromVizuryHandler = async (notificationPayload) => {
        if (Platform.OS === 'android') {
            //sendingmap is data that is obtained from remoteMessage (in map format)
            let sendingMap = {};
            sendingMap["attributes"] = notificationPayload.data;
            const isPushFromVizury = await RnVizuryLogger.isPushFromVizury(sendingMap);
            
            if (isPushFromVizury) {
                RnVizuryLogger.handleNotificationReceived(sendingMap);
            } else {
                // your own logic goes here
            }
        }
    }

```


## iOS Integration



First open Pod file in your iOS app and add these lines 

```javascript
use_frameworks!
pod 'Firebase/Messaging'
```

Then open terminal and go to project folder then do following steps to install pod file



```
$ cd ios
$ pod install
```

If you find any issue in pod install please try with ```pod install --repo-update```.

## Vizury SDK Initialization

In your AppDelegate file import the VizuryEventLogger

#### Objective-C
----
```objc
#import "RnVizuryLogger.h"
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

// Implement UNUserNotificationCenterDelegate to receive display notification via APNS for devices
// running iOS 10 and above.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate>
@end
#endif

// Copied from Apple's header in case it is missing in some cases (e.g. pre-Xcode 8 builds).
#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif
```

Add the following in `didFinishLaunchingWithOptions` method of AppDelegate to initialize the SDK

```objc
[RnVizuryLogger initializeEventLoggerInApplication:(UIApplication*)application
                            WithPackageId:(NSString *)packageId
                            ServerURL:(NSString *)serverURL
                            WithCachingEnabled:(BOOL) caching
                            AndWithFCMEnabled:(BOOL) isFCMEnabled];
```

#### Swift
---- 
```objc
#import RnVizuryLogger
```

Update your AppDelegate

```swift
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate
```

Add the following in `didFinishLaunchingWithOptions` method of AppDelegate to initialize the SDK

```objc
RnVizuryLogger.initializeEventLogger(in: application,
            withPackageId: packageId, 
            serverURL: serverUrl,
            withCachingEnabled: caching, 
            AndWithFCMEnabled: isFCMEnabled)
```

```
Where 
  packageId     : packageId obtained from vizury
  serverURL     : serverURL obtained from vizury
  caching       : pass true if your app supports offline usage and you want to send user behaviour data 
                  to vizury while he was offline. Pass false otherwise
  isFCMEnabled  : true/false depending on if you want to use vizury for push
Thats it!! SDK is successfully integrated and initialized in the project, and ready to use.
```


#### Enabling FCM

Create a Firebase project in the [Firebase console](https://console.firebase.google.com/u/0/) if you don't already have one. Enter the `Project-Name`.

![createProject-1](resources/firebase1.png)

Click on `iOS` option and in the next screen add the `iOS Bundle Id`. The `iOS Bundle Id` should be same as your apps bundle identifier. You can download the `GoogleService-Info.plist` file in the next step.

![createProject-2](resources/firebase2.png)

`Note : The GoogleService-Info.plist file that you have downloaded will have certain settings like IS_ADS_ENABLED, IS_SIGNIN_ENABLED set as YES. You have to add correspinding pod dependencies for the same or you can turn them off if you are not using them`

Next click on the `Settings icon` option of the created project.

![createProject-3](resources/firebase3.png )

Click on `Cloud Messaging` tab and upload APNs Authentication Key (.p8 format). Also note down the the `Server key` as this will be required later during the integration. You can also upload APNs certificaties but configuration with auth keys is recommended as they are the more current method for sending notifications to iOS

![createProject-4](resources/firebase4.png )

While uploading APNs Authentication Key (.p8 format) you need to enter the Key Id and Team Id. 
1. 'Key ID' is the id of the authentication key you created in Apple developer console under `Certificates, Identifiers & Profiles` -> Keys -> Select the particular certificate.

![createProject-4](resources/keyID.png )

2. 'Team ID' is the Team Member ID in Apple developer console under Membership -> Membership Details

![createProject-4](resources/teamID.png )

### Configuring Application

* Drag the GoogleService-Info.plist file you just downloaded into the root of your Xcode project and add it to all targets
* Register for Push notifications inside `didFinishLaunchingWithOptions` method of you `AppDelegate`

```objc
if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
    // iOS 7.1 or earlier. Disable the deprecation warnings.
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wdeprecated-declarations"
    UIRemoteNotificationType allNotificationTypes =
    (UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge);
    [application registerForRemoteNotificationTypes:allNotificationTypes];
    #pragma clang diagnostic pop
} else {
    // iOS 8 or later
    // [START register_for_notifications]
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
        #if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
        #endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
  
  }
```


### Getting Notification from FCM


```objc
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {

  // didReceiveLocalNotification Code Here

}
```

```objc
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {

    [application registerForRemoteNotifications];

}
```

`// iOS 7 or iOS 6`
```objc
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
  NSLog(@"token %@",token);
  [RnVizuryLogger passAPNSToken:deviceToken];
    // Send token to server
} 
```

```objc
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [RnVizuryLogger didFailToRegisterForPush];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
[RnVizuryLogger didReceiveRemoteNotificationInApplication:application withUserInfo:userInfo];

    if(application.applicationState == UIApplicationStateInactive) {
        NSLog(@"Appilication Inactive - the user has tapped in the notification when app was closed or in background");
        [self customPushHandler:userInfo];
    }
}
```
```objc
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
            willPresentNotification:(UNNotification *)notification
            withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    NSLog(@"%@", userInfo);
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionNone);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
                didReceiveNotificationResponse:(UNNotificationResponse *)response
                withCompletionHandler:(void (^)(void))completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    [RnVizuryLogger didReceiveResponseWithUserInfo:userInfo];
    
    // Print full message.
    NSLog(@"%@", userInfo);
    completionHandler();
}
```

```objc
- (void) customPushHandler:(NSDictionary *)notification {
    if (notification !=nil && [notification objectForKey:@"deeplink"] != nil) {
        NSString* deeplink = [notification objectForKey:@"deeplink"];
        NSLog(@"%@",deeplink);
        // Here based on the deeplink you can open specific screens that's part of your app
    }
}
```

## Android Integration


#### Vizury SDK Initialization

In your `android/app/src/main/AndroidManifest.xml`

```xml
    .....
        <meta-data
            android:name="Vizury.VRM_ID"
            android:value="{PACKAGE_ID}" />
        <meta-data
            android:name="Vizury.SERVER_URL"
            android:value="{SERVER_URL}" />
        <meta-data
             android:name="Vizury.DEBUG_MODE"
             android:value="true" />
        <meta-data
            android:name="Vizury.NOTIFICATION_ICON"
            android:resource="@mipmap/ic_launcher" />
        <meta-data
            android:name="Vizury.NOTIFICATION_ICON_SMALL"
            android:resource="@mipmap/ic_launcher" />
        <service
            android:name="com.vizury.mobile.Push.VizFirebaseMessagingService">
            <intent-filter>
                <action android:name="com.google.firebase.MESSAGING_EVENT"/>
            </intent-filter>
        </service>
        <service
            android:name="com.vizury.mobile.Push.VizIntentService"
            android:exported="false">
        </service>
        <meta-data
            android:name="Vizury.DISABLE_SCHEDULER"
            android:value="true" />
    .....
```
`PACKAGE_ID` and `SERVER_URL` will be provided by the vizury account manager. `DEBUG_MODE` is used to show vizury debug/error/info logs. Set this to false when going to production.

In your `android/app/build.gradle`

```gradle
dependencies {
  ...
  implementation 'com.google.firebase:firebase-messaging:17.3.4'
  implementation 'com.android.support.constraint:constraint-layout:1.1.3'
  ...

  apply plugin: 'com.google.gms.google-services'
}
```

In your `android/build.gradle`

```gradle
buildscript {
    ...
    dependencies {
        ...
        classpath 'com.google.gms:google-services:4.2.0'
        ...
    }
}
```

Then put your `google-services.json` in `android/app/`.

## Android Impression and Click Tracking (Without the interference of vizury SDK)

### Impression Tracking

In Case the Notification is built by Application Code -  without Vizury SDK (OS - Android only)

On receiving the payload from FCM Server  , Intercept the  received json and get the impression url from the payload using the key "impr" and pass it to below method to get the complete url and fire it


```
var impessionUrl = await RnVizuryLogger.getImpressionUrl(IMPRESSION_URL);
console.log("Impression Url: " + impessionUrl);
# fire the impessionUrl using xmlhttp request or on your choice
```

### Click Tracking

In Case the Notification Click is interacted by Application Code -  without Vizury SDK (OS - Android only)

On receiving the payload from FCM Server Store the payload locally in shared pref(which ever suits your use case)  , On click get the payload from storage Intercept it and get the click url from the payload using the key "click" and pass it to below method to get the complete url and fire it

```
var clickUrl = await RnVizuryLogger.getClickUrl(CLICK_URL);
console.log("Click Url: " + clickUrl);
# fire the clickUrl using xmlhttp request or on your choice
```



